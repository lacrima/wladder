#!/usr/bin/env python
'''`Word ladder puzzle`__ solver.

__ http://en.wikipedia.org/wiki/Word_ladder

It works under Python >= 2.6 (including Python >= 3).

Usage from Python shell:

.. sourcecode:: python

    >>> import ladder
    >>> distance, path = ladder.ladder('oat', 'rye')
    >>> distance
    3
    >>> path
    ['oat', 'rat', 'rae', 'rye']

Basic usage from command line::

    $ ./ladder.py chin nose
    CHIN NOSE 5

You can also pipe to the stdin of ``ladder.py``::

    $ echo pitch tents | ./ladder.py
    PITCH TENTS 11

Or redirect its stdin::

    $ cat > myfile
    chin nose
    pity good
    oat rye
    $ ./ladder.py < myfile
    CHIN NOSE 5
    PITY GOOD 7
    OAT RYE 3

To output the whole path use ``--verbose`` option::

    $ ./ladder.py --verbose pitch tents
    PITCH -> PATCH -> PARCH -> PERCH -> PEACH -> PEACE -> PEALE -> PEALS ->
    MEALS -> MEATS -> TEATS -> TENTS 11

You can also provide your own dictionary of words. On many linux systems there
are some standard dictionaries of words located under ``/usr/share/dict``::

    $ ./ladder.py --dict /usr/share/dict/words oat rye
    OAT RYE 3

Running ``ladder.py`` with ``-h`` option outputs help message.
'''
from __future__ import print_function
import sys, string, optparse
from heapq import heappop, heappush

words = None

def find_distance(word, target):
    '''Finds the Hamming distance between two words.

    We will use this function as heuristic to solve word ladder.
    See http://en.wikipedia.org/wiki/Hamming_distance
    '''
    return sum(char1 != char2 for char1, char2 in zip(word, target))


def get_neighbors(word):
    '''A generator of similar words.

    Credit goes to Alex Martelli for this function
    See http://stackoverflow.com/a/3031104/347735
    '''
    wordl = list(word)
    for i, word_char in enumerate(wordl):
        for ascii_char in string.ascii_lowercase:
            if ascii_char == word_char: continue
            wordl[i] = ascii_char
            new_word = ''.join(wordl)
            if new_word in words: yield new_word
        wordl[i] = word_char


def ladder(from_word, target):
    '''Uses A* algorithm to find the shortest path to target word.

    http://en.wikipedia.org/wiki/A*_search_algorithm
    '''
    assert len(from_word) == len(target), "Words must be of equal length"
    heuristic = lambda word: find_distance(word, target)

    came_from = {}
    queue = [(heuristic(from_word), None, from_word)]

    while queue:
        distance, prev, current = heappop(queue)

        if current in came_from: continue
        came_from[current] = prev

        if current == target:
            return distance, reconstruct_path(came_from, target)

        for neighbor in get_neighbors(current):
            # This is distance-plus-cost heuristic for our ladder,
            # i.e. f(x) = g(x) + h(x)
            # see http://en.wikipedia.org/wiki/A*_search_algorithm#Description
            new_distance = distance + 1 - heuristic(current) + \
                heuristic(neighbor)
            heappush(queue, (new_distance, current, neighbor))

    return float('inf'), None # no solution found


def reconstruct_path(came_from, current_node):
    '''Reconstructs the path from came_from dictionary'''
    parent = came_from.get(current_node)
    if parent:
        path = reconstruct_path(came_from, parent)
        return path + [current_node]
    else: return [current_node]


def main():
    '''Executed when we run ladder.py from command line'''
    global words
    arg_parser = optparse.OptionParser(description="Word-ladder solver",
        usage="usage: %prog [options] start_word end_word")
    arg_parser.add_option('-d', '--dict', metavar='path',
        help="provide your own dictionary file (one word per line)")
    arg_parser.add_option('-v', '--verbose', action='store_true',
        help='output the whole ladder, e.g. "OAT -> RAT -> RAE -> RYE 3"')
    options, args = arg_parser.parse_args()

    # stdin is connected to a terminal
    if sys.stdin.isatty():
        if len(args) != 2:
            arg_parser.error("You need to provide two positional arguments: "
                             "start_word and end_word")
        inputs_list = [(args[0].lower(), args[1].lower())]

    else: # stdin is redirected/piped
        inputs_list = [line.strip().lower().split() for line in sys.stdin]

    words = set(w.strip().lower() for w in open(options.dict or 'english'))

    for inputs in inputs_list:

        not_in_words = set(inputs) - words
        if not_in_words:
            sys.exit("Words {0} are not in dictionary".format(not_in_words))

        try:
            distance, path = ladder(*inputs)
        except AssertionError as e:
            # words must be of equal length
            sys.exit(e)

        if not path:
            sys.exit("No ladder for this input: {0} {1}".format(
                inputs[0].upper(), inputs[1].upper()))

        # make words uppercase
        path = list(map(str.upper, path))

        if options.verbose:
            print(' -> '.join(path), distance)
        else: print(path[0], path[-1], distance)


if __name__ == '__main__':
    main()
else:
    # set defalt dictionary in the case we are not executing ladder.py as
    # a script, but importing it instead.
    words = set(w.strip().lower() for w in open('english'))
