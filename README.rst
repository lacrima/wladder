`Word ladder puzzle`__ solver.

__ http://en.wikipedia.org/wiki/Word_ladder

It works under Python >= 2.6 (including Python >= 3).

Usage from Python shell:

.. sourcecode:: python

    >>> import ladder
    >>> distance, path = ladder.ladder('oat', 'rye')
    >>> distance
    3
    >>> path
    ['oat', 'rat', 'rae', 'rye']

Basic usage from command line::

    $ ./ladder.py chin nose
    CHIN NOSE 5

You can also pipe to the stdin of ``ladder.py``::

    $ echo pitch tents | ./ladder.py
    PITCH TENTS 11

Or redirect its stdin::

    $ cat > myfile
    chin nose
    pity good
    oat rye
    $ ./ladder.py < myfile
    CHIN NOSE 5
    PITY GOOD 7
    OAT RYE 3

To output the whole path use ``--verbose`` option::

    $ ./ladder.py --verbose pitch tents
    PITCH -> PATCH -> PARCH -> PERCH -> PEACH -> PEACE -> PEALE -> PEALS ->
    MEALS -> MEATS -> TEATS -> TENTS 11

You can also provide your own dictionary of words. On many linux systems there
are some standard dictionaries of words located under ``/usr/share/dict``::

    $ ./ladder.py --dict /usr/share/dict/words oat rye
    OAT RYE 3

Running ``ladder.py`` with ``-h`` option outputs help message.

